// js ===> version ===>> ECMA
// ECMAScript

// ES
// stable JS ==> ES5

// Modern JS ==> ES6 and beyond


// To be covered
// Object shorthand
// object destruction
// spread operator and rest operator
// arrow notaion function
// default argument
// import and export
// template literals
// class
// datatype


// Object Short hand
// var xyz = abcd
// var obj ={
//     name :'sandesh',
//     phone : 333,
//     xyz,
// }
// console.log('object is ==>',obj)

// object destruction

var fish = 'golden fish'
 
function getSomething(){
    
    return {
        fruits:['apple'],
        vegetable:['potato'],
        fish:'cat fish',
        abc:'xyz',
        flower:'rose'
        
    }
}

// destruction
var {flower} = getSomething();

// destruction with own name
var {fish:myfish} = getSomething();

// console.log('result is >>',fruits)
// console.log('flower >>',flower)
console.log('fish is >>',fish)
console.log('myfish is >>',myfish)


// arrow notation function

function welcome(name){
    return 'hello'+name;
}

// const welcome = (name) =>{
//     return 'hello' + name;
// }

// for single argument parenthesis is optional
// const welcome = name => {
//     return 'hello' + name;

// }
// we can skip(avoid) middlebraces and return statement
// one liner function
// const welcomeArr = name => 'hello' + name

// console.log('welcome traditional',welcome('broadway'))
// console.log('welcome one liner using arrow>>',welcomeArr('infosys'))

// main advantages
// ===> arrow notation function will inherit parent scope(this)


const laptops = [
    {
        brand :'dell'
    },
    {
        brand :'dell'
    },
    {
        brand :'hp'
    },
    {
        brand :'hp'
    }
]

var delllaptop = laptops.filter(function(item){
    if (item.brand === 'dell'){
        return true;
    }
})
console.log('dell laptops >>',delllaptop)

const dell = laptops.filter(data=>data.brand==='dell');
console.log('dell>>',dell)


const multiply = num1 => num2 => num1 * num2;
console.log('multiply >>',multiply(4)(3))


// spread operator and rest operator
//   ...

// spread operator  --> value 1 by 1 spread garne
// existing behaviour ==> mutable behaviour for array and object
 
// var obj1 = {
//     name: 'braodway infosys nepal',
//     address:'tinkune',
//     shift:'morning'
// }

// var student = {
//     course:'mern',
//     shift:'evening'
// }

// var obj2 = {
//     ... student,
//     ... obj1,
// };
// obj1.phone = '3333'

// console.log('obje2 >>',obj2);
// console.log('obj 1>>', obj1);

// rest operator (destruction ma used huncha)

// var bike = {
//     color:'black',
//     model:'gs',
//     brand:'BMW',
//     cc:'310',
// }
// const{model:Model,cc, ...rest} = bike;

// console.log('model , cc',Model,cc)
// console.log('rest',rest)


// default argument

// function sendMail(details={
//     name:'sandesh'
// }){
//     var message = 'hi ' + details.name + ' welcome to braodway';
//     console.log('message >',message)
// }
// sendMail()


// function testa(name='rupak'){
//     console.log('name',name);
// }
// testa('sita')


// import and export

// export
// two types of export
// 1. named export
// 2. default export

// syntax 
// named export
// export const test = 'value'
// export const email = 'sadasdqrf';
// export class fruits{}

// there can be multiple named export wwithin a file

// 2. default export
// syntax
// export default any data type
//  eg export default [];
// eg export default {};
// eg export default 'hi'
// eg export default 33;

// there can be only one default export
// there can be both named and default export

// import
// import totally depends on how it is exported
// if  named export
// syntax
// import {named_export_1, named_export_2} from 'source' // source can be own file or node_modules packages


// if default export
// import xyzName(anyName) from 'source'

// if both name and default export are present
// import{name2,name1},DefaultName from 'source

// template literals

// string and string concatination

// syntax ``
// var message = 'hi ' +name+ ' welcome to bradway'

var name = 'sandesh'
var text= `hi
${name} 
welcome to 
bradway`;
console.log('text is >>',text)


// block scope
// let holds block scope //es6

// class

// class based OOP

// class is basic building block of class based OOP

// Class is group of constructor , methods and properties (fields)

class Auth {
    isLoggedIn;
    status;
    constructor(){
        this.isLoggedIn = true; // hamro class bhitra jati pani properties ra methods haru add garchau tyo property ra method haru within a class this a available huncha
        this.status='online'
    }

    getStatus(){
        return this.status;
    }

    setStatus(newStatus){
        this.status = newStatus;
        return this.status;
    }
}
// inheritance

class B extends Auth{
    bbb;
    constructor(){
        super(); // super call is parent class constructor call

    }
    getBBB();
}

// var abc = new B();
// abc.

// var xyz = new Auth();
// xyz.

// datatype
// symbol, ==> unique value
// bigint ===> to store large value

// pre-requisities to learn React
// JSX  ===> JavaScript Extended syntax
// Compiler
// Task Runner
