// main goal is to create own element

const container =document.getElementById('test');

// component is React
// component can be written as class based and functional

// eg of functional component
// component must return single html node

function Welcome(args){
    console.log('arg is >>',args);
    return <p>I am JSX block of code inside a component</p>
}

ReactDOM.render(<Welcome age='333' phone='3990' status='online'></Welcome>, container)